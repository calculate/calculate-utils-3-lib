#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# setup.py --- Setup script for calculate-lib

# Copyright 2008-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys, os
from distutils.core import setup
from distutils.command.build_scripts import build_scripts
from distutils.command.install_scripts import install_scripts

__version__ = "3.2.2"
__app__ = "calculate-lib"

try:
    from portage.const import EPREFIX
except ImportError:
    EPREFIX=''

module_name = "calculate.lib"

packages = [
    str('.'.join(root.split(os.sep)[1:]))
    for root, dirs, files in os.walk('pym/calculate')
    if '__init__.py' in files
]

setup(
    name = __app__,
    version = __version__,
    description = "The library for Calculate 3",
    author = "Mir Calculate Ltd.",
    author_email = "support@calculate.ru",
    url = "http://calculate-linux.org",
    license = "http://www.apache.org/licenses/LICENSE-2.0",
    scripts = [],
    package_dir={'': 'pym'},
    packages=packages,
    data_files = [("/etc/calculate", []),
                  ("/var/log/calculate", [])]
)
