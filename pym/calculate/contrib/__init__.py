import site
import os
import sys


current_version = ".".join(map(str, sys.version_info[0:2]))

if not any("calculate/contrib" in x for x in sys.path):
    for sitedir in site.getsitepackages():
        if sitedir.endswith(f"python{current_version}/site-packages"):
            for i, syspathdir in enumerate(sys.path):
                if syspathdir.endswith(f"python{current_version}/site-packages"):
                    contribpath = "%s/calculate/contrib" % syspathdir
                    if os.path.exists(contribpath):
                        sys.path.insert(i, contribpath)
                        break
    if not any("calculate/contrib" in x for x in sys.path):
        raise ImportError("Failed to install calculate contribution directory")
