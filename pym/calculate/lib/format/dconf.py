# -*- coding: utf-8 -*-

# Copyright 2008-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from ..cl_template import TemplateFormat
from ..utils.files import process, STDOUT, getProgPath
from os import path

from ..cl_lang import setLocalTranslate

_ = lambda x: x
setLocalTranslate('cl_lib3', sys.modules[__name__])


class dconf(TemplateFormat):
    """
    Формат dconf, каждый шаблон является входящими данными
    для команды dconf load <dconfpath>
    """
    dconfpath = "/"
    user = "root"

    def prepare(self):
        self.run_session = getProgPath('/usr/bin/dbus-run-session')
        self.su = getProgPath('/bin/su')
        self.dconf = getProgPath('/usr/bin/dconf')

    def setUser(self, user):
        self.user = user

    def setPath(self, dconfpath):
        self.dconfpath = path.normpath(dconfpath)
        if not self.dconfpath.endswith("/"):
            self.dconfpath = "%s/" % self.dconfpath

    def textToXML(self):
        return self.text

    def processingFile(self, textConfigFile, rootPath=None, nameFile=None):
        if not (self.run_session and self.su and self.dconf):
            self.setError(_("The 'dconf' format is unavailable"))
            return False
        dconfRun = process(self.su, self.user, "-c",
                           "%s %s load %s" % (self.run_session,
                                              self.dconf,
                                              self.dconfpath), stderr=STDOUT)
        dconfRun.write(self.text)
        if dconfRun.success():
            return "OK"
        else:
            for line in dconfRun:
                if "error" in line:
                    self.setError(line.strip())
                    return False
            self.setError(_("Config is wrong"))
            return False
