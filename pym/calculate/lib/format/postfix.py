# -*- coding: utf-8 -*-

# Copyright 2008-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import re
from ..cl_xml import xmlDoc
from .apache import apache


class postfix(apache):
    """Класс для обработки конфигурационного файла типа postfix

    """
    _comment = "#"
    configName = "postfix"
    configVersion = "0.1"
    sepFields = "\n"
    reComment = re.compile("[ \t]*%s" % _comment)
    reSepFields = re.compile(sepFields)
    # разделитель названия и значения переменной
    reSeparator = re.compile("\s*=\s*")

    def prepare(self):
        # Объект документ
        self.docObj = self.textToXML()
        # Создаем поля разделенные массивы
        self.docObj.postParserListSeplist(self.docObj.body)
        # XML документ
        self.doc = self.docObj.doc

    def join(self, postfixObj):
        """Объединяем конфигурации"""
        if isinstance(postfixObj, postfix):
            self.docObj.joinDoc(postfixObj.doc)

    def textToXML(self):
        """Преобразуем текст в XML"""

        class area:
            def __init__(self):
                self.header = False
                self.start = False
                self.fields = []
                self.end = False

        areas = []
        oneArea = area()
        oneArea.header = ""
        oneArea.start = ""
        oneArea.fields = [self.text]
        oneArea.end = ""
        areas.append(oneArea)
        docObj = xmlDoc()
        # Создание объекта документ c пустым разделителем между полями
        docObj.createDoc(self.configName, self.configVersion)
        if not areas:
            return docObj
        self.createXML(areas, docObj.getNodeBody(), docObj)
        return docObj

    def setDataField(self, txtLines, endtxtLines):
        """Cоздаем список объектов с переменными"""

        class fieldData:
            def __init__(self):
                self.name = False
                self.value = False
                self.comment = False
                self.br = False

        fields = []
        field = fieldData()
        z = 0
        for k in txtLines:
            textLine = k + endtxtLines[z]
            z += 1
            findComment = self.reComment.search(textLine)
            if not textLine.strip():
                field.br = textLine
                fields.append(field)
                field = fieldData()
            elif findComment:
                field.comment = textLine
                fields.append(field)
                field = fieldData()
            else:
                pars = textLine.strip()
                nameValue = self.reSeparator.split(pars)
                if len(nameValue) == 1:
                    field.name = ""
                    field.value = textLine.replace(self.sepFields, "")
                    field.br = textLine
                    fields.append(field)
                    field = fieldData()

                if len(nameValue) > 2:
                    valueList = nameValue[1:]
                    nameValue = [nameValue[0], "=".join(valueList).replace(
                        self.sepFields, "")]
                if len(nameValue) == 2:
                    name = nameValue[0]
                    value = nameValue[1].replace(self.sepFields, "")
                    field.name = name.replace(" ", "").replace("\t", "")
                    field.value = value
                    field.br = textLine
                    fields.append(field)
                    field = fieldData()
        return fields
