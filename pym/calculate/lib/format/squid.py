# -*- coding: utf-8 -*-

# Copyright 2008-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import re
from .procmail import procmail


class squid(procmail):
    """Класс для обработки конфигурационного файла типа  squid

    """
    configName = "squid"
    configVersion = "0.1"
    # разделитель названия и значения переменной
    reSeparator = re.compile(" ")

    def prepare(self):
        super().prepare()
        # Создаем поля-массивы
        self.docObj.postParserList()
        # Создаем поля разделенные массивы
        self.docObj.postParserListSeplist(self.docObj.body)

    def setDataField(self, txtLines, endtxtLines):
        """Создаем список объектов с переменными"""

        class fieldData:
            def __init__(self):
                self.name = False
                self.value = False
                self.comment = False
                self.br = False

        fields = []
        field = fieldData()
        z = 0
        for k in txtLines:
            textLine = k + endtxtLines[z]
            z += 1
            findComment = self.reComment.search(textLine)
            flagVariable = True
            if not textLine.strip():
                field.br = textLine
                fields.append(field)
                field = fieldData()
                flagVariable = False
            elif findComment:
                if textLine[:findComment.start()].strip():
                    field.comment = textLine[findComment.start():]
                    textLine = textLine[:findComment.start()]
                else:
                    field.comment = textLine
                    flagVariable = False
                fields.append(field)
                field = fieldData()
            if flagVariable:
                pars = textLine.strip()
                nameValue = self.reSeparator.split(pars)
                if len(nameValue) > 2:
                    valueList = nameValue[1:]
                    nameValue = [nameValue[0], " ".join(valueList)]
                if len(nameValue) == 2:
                    name = nameValue[0]
                    value = nameValue[1].replace(self.sepFields, "")
                    field.name = name.replace(" ", "").replace("\t", "")
                    field.value = value
                    field.br = textLine
                    fields.append(field)
                    field = fieldData()
        return fields

    def join(self, squidObj):
        """Объединяем конфигурации"""
        if isinstance(squidObj, squid):
            self.docObj.joinDoc(squidObj.doc)
