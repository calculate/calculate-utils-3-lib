# -*- coding: utf-8 -*-

# Copyright 2008-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import re
from .procmail import procmail


class openrc(procmail):
    """Класс для обработки конфигурационного файла типа  openrc

    """
    _comment = "#"
    configName = "openrc"
    configVersion = "0.1"
    sepFields = "\n"
    reComment = re.compile("[ \t]*%s" % _comment)
    reSepFields = re.compile(sepFields)
    # разделитель названия и значения переменной
    dataFieldSeparator = "="

    def setDataField(self, txtLines, endtxtLines):
        """Создаем список объектов с переменными"""

        class fieldData:
            def __init__(self):
                self.name = False
                self.value = False
                self.comment = False
                self.br = False

        fields = []
        field = fieldData()
        z = 0
        for k in txtLines:
            textLine = k + endtxtLines[z]
            z += 1
            findComment = self.reComment.search(textLine)
            if not textLine.strip():
                field.br = textLine
                fields.append(field)
                field = fieldData()
            elif findComment:
                field.comment = textLine
                fields.append(field)
                field = fieldData()
            else:
                pars = textLine.strip()
                nameValue = pars.partition(self.dataFieldSeparator)
                if nameValue[1] == self.dataFieldSeparator:
                    name = nameValue[0]
                    value = nameValue[2].replace(self.sepFields, "")
                    field.name = (
                    name.replace(" ", "").replace("\t", "")).lower()
                    field.value = value
                    field.br = textLine
                    fields.append(field)
                    field = fieldData()
        return fields

    def join(self, openrcObj):
        """Объединяем конфигурации"""
        if isinstance(openrcObj, openrc):
            self.docObj.joinDoc(openrcObj.doc)
