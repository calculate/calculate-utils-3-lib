# -*- coding: utf-8 -*-

# Copyright 2008-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import re
from ..cl_xml import xpath
from .generic import objShare
from ..cl_template import blocText
from ..cl_xml import xmlDoc, insertBefore


class samba(objShare):
    """Класс для обработки конфигурационного файла типа samba

    """
    _comment = "#"
    configName = "samba"
    configVersion = "0.1"
    reHeader = re.compile("^[\t ]*\[[^\[\]]+\].*\n", re.M)
    reBody = re.compile(".+", re.M | re.S)
    reComment = re.compile("\s*%s.*|\s*;.*" % _comment)
    reSeparator = re.compile("\s*=\s*")
    sepFields = "\n"
    reSepFields = re.compile(sepFields)

    def prepare(self):
        self.blocTextObj = blocText()
        self._splitToFields = self.splitToFields
        # Объект документ
        self.docObj = self._textToXML()
        # XML документ
        self.doc = self.docObj.doc

    def postXML(self):
        """Последующая постобработка XML"""
        # удаляем пустые области
        xmlAreas = xpath.Evaluate("child::area", self.docObj.body)
        removeList = []
        for xmlArea in xmlAreas:
            xmlFields = xpath.Evaluate("child::field/name", xmlArea)
            if not xmlFields:
                removeList.append(xmlArea)
        for xmlArea in removeList:
            parentNode = xmlArea.getparent()
            parentNode.remove(xmlArea)

        # Для добавления перевода строки между областями если его нет
        xmlAreas = xpath.Evaluate("child::area", self.docObj.body)
        xmlArea = None
        for xmlArea in xmlAreas:
            xmlFields = xpath.Evaluate("child::field", xmlArea)
            if not (xmlFields and (
                self.docObj.getTypeField(xmlFields[-1]) == "br" or
                    self.docObj.getTypeField(xmlFields[-1]) == "comment")):
                if xmlArea.getnext() is not None:
                    parentNode = xmlArea.getparent()
                    nextNode = xmlArea.getnext()
                    insertBefore(parentNode, self.docObj.createField(
                        "br", [], "", [], False, False), nextNode)
            # Добавление переводов строк между полями
            if xmlFields:
                for node in xmlFields:
                    # Добавление перевода строк в если его нет между полями
                    if (self.docObj.getTypeField(node) == "var" and
                            node.getprevious() is not None and
                            not (self.docObj.getTypeField(
                                node.getprevious()) in ("br", "comment"))):
                        insertBefore(xmlArea, self.docObj.createField(
                            "br", [], "", [], False, False), node)

        # Удаление лишних переводов строк
        childNodes = self.docObj.getFieldsArea(self.docObj.body)
        lenBr = 0
        removeBrNodes = []
        for node in childNodes:
            if (node.tag == "field" and
                    self.docObj.getTypeField(node) == "br"):
                lenBr += 1
                if lenBr > 2:
                    removeBrNodes.append(node)
            else:
                lenBr = 0
        # Удаление
        for rmNode in removeBrNodes:
            self.docObj.body.remove(rmNode)

        # Если после есть BR а за ним ничего нет, удаляем BR
        if xmlArea is not None:
            if (xmlArea.getnext() is not None and
                    self.docObj.getTypeField(xmlArea.getnext()) == "br"):
                if xmlArea.getnext().getnext() is None:
                    parentNode = xmlArea.getnext().getparent()
                    parentNode.remove(xmlArea.getnext())

    def join(self, sambaObj):
        """Объединяем конфигурации"""
        if isinstance(sambaObj, samba):
            self.docObj.joinDoc(sambaObj.doc)
            self.postXML()

    def setDataField(self, txtLines, endtxtLines):
        """Создаем список объектов с переменными"""

        class fieldData:
            def __init__(self):
                self.name = False
                self.value = False
                self.comment = False
                self.br = False

        fields = []
        field = fieldData()
        z = 0
        for k in txtLines:
            textLine = k + endtxtLines[z]
            z += 1
            findComment = self.reComment.search(textLine)
            if not textLine.strip():
                field.br = textLine
                fields.append(field)
                field = fieldData()
            elif findComment:
                field.comment = textLine
                fields.append(field)
                field = fieldData()
            else:
                pars = textLine.strip()
                nameValue = self.reSeparator.split(pars)
                if len(nameValue) > 2:
                    valueList = nameValue[1:]
                    nameValue = [nameValue[0], "=".join(valueList)]
                if len(nameValue) == 2:
                    name = nameValue[0]
                    value = nameValue[1].replace(self.sepFields, "")
                    field.name = name.replace(" ", "").replace("\t", "")
                    field.value = value
                    field.br = textLine
                    fields.append(field)
                    field = fieldData()
        return fields

    def splitCleanBloc(self, txtBloc):
        """Делим блок на две части (переменные, пустые строки в конце)"""
        txtLines = txtBloc.split("\n")
        nextBloc = []
        txtLines.reverse()
        z = 0
        for txtLine in txtLines:
            if not txtLine.strip():
                nextBloc.append(txtLine)
            else:
                break
            z += 1
        txtLines.reverse()
        firstBloc = txtLines[:-z]
        nextBloc.reverse()
        if nextBloc:
            firstBloc.append("")
        if nextBloc and "\n".join(nextBloc):
            return "\n".join(firstBloc), "\n".join(nextBloc)
        else:
            return False

    def getFullAreas(self, blocs):
        """Делит текст на области, (Заголовок, тело)

        Возвращает два списка: заголовки, тела
        """
        headsAreas = []
        bodyAreas = []
        if not blocs:
            return []
        lenBlocs = len(blocs[0])
        for i in range(lenBlocs):
            txtBloc = blocs[1][i]
            clean = self.splitCleanBloc(txtBloc)
            if clean:
                headsAreas.append(blocs[0][i])
                bodyAreas.append(clean[0])
                headsAreas.append("")
                bodyAreas.append(clean[1])
            else:
                headsAreas.append(blocs[0][i])
                bodyAreas.append(blocs[1][i])
        return headsAreas, bodyAreas

    def createTxtConfig(self, strHeader, dictVar):
        """Cоздает область с заголовком

        создает текст конфигурационного файла в формате samba из
        заголовка (строка) и словаря переменных
        """
        if not strHeader:
            return ""
        outTxt = "[" + strHeader + "]\n"
        for key in dictVar.keys():
            outTxt += "%s = %s\n" % (key, dictVar[key])
        return outTxt

    def _textToXML(self):
        """Преобразует текст в XML"""
        blTmp = self.blocTextObj.findBloc(self.text, self.reHeader, self.reBody)
        blocs = self.getFullAreas(blTmp)
        headers = []
        startHeaders = []
        finHeaders = []
        docObj = xmlDoc()
        docObj.createDoc(self.configName, self.configVersion)
        rootNode = docObj.getNodeBody()
        # Если пустой текст то создаем пустой документ
        if not blocs:
            return docObj

        for h in blocs[0]:
            listfinH = h.split("]")
            finH = listfinH[0]
            if "[" in finH:
                startHeaders.append(finH + "]")
            else:
                startHeaders.append(finH)
            if len(listfinH) == 2:
                finHeaders.append(listfinH[1])
            else:
                finHeaders.append("")
            headers.append(finH.replace("[", "").replace("]", "").strip())
        bodys = blocs[1]

        z = 0
        for h in headers:
            if not bodys[z]:
                z += 1
                continue
            areaAction = False
            if h:
                if h[0] == "!":
                    docObj.createCaption(h[1:], [startHeaders[z], ""])
                    areaAction = "drop"
                elif h[0] == "-":
                    docObj.createCaption(h[1:], [startHeaders[z], ""])
                    areaAction = "replace"
                else:
                    docObj.createCaption(h, [startHeaders[z], ""])
            else:
                docObj.createCaption(h, [startHeaders[z], ""])

            if "\n" in blocs[0][z]:
                if self.reComment.search(finHeaders[z]):
                    docObj.createField('comment', [finHeaders[z]])
                elif not finHeaders[z].strip() and \
                        finHeaders[z].replace("\n", ""):
                    docObj.createField('br',
                                       [finHeaders[z].replace("\n", "")])
                else:
                    docObj.createField('br')
            fields = self._splitToFields(bodys[z])
            for f in fields:
                if (f.name and f.value is not False and f.br):
                    # Обработка условий для samba
                    if (f.name[0] == "!" or
                            f.name[0] == "-" or f.name[0] == "+"):
                        qns = self.removeSymbolTerm(f.br)
                        xmlField = docObj.createField(
                            "var", [qns], f.name[1:], [f.value])
                        if f.name[0] == "!":
                            # Удаляемое в дальнейшем поле
                            docObj.setActionField(xmlField, "drop")
                    else:
                        docObj.createField("var", [f.br.replace("\n", "")],
                                           f.name, [f.value])
                    docObj.createField('br')
                elif f.comment is not False:
                    docObj.createField('comment', [f.comment])
                elif f.br is not False:
                    docObj.createField('br', [f.br.replace("\n", "")])
            if h.strip():
                area = docObj.createArea()
                if areaAction:
                    docObj.setActionArea(area, areaAction)
                rootNode.append(area)
            else:
                fieldsNodes = docObj.tmpFields.getFields()
                for fieldNode in fieldsNodes:
                    rootNode.append(fieldNode)
                docObj.clearTmpFields()
            z += 1
        return docObj
