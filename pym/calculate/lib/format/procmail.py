# -*- coding: utf-8 -*-

# Copyright 2008-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import re
from ..cl_xml import xpath, xmlDoc, insertBefore
from .generic import objShare

class procmail(objShare):
    """Класс для обработки конфигурационного файла типа  procmail

    """
    _comment = "#"
    configName = "procmail"
    configVersion = "0.1"
    sepFields = "\n"
    reComment = re.compile("[ \t]*%s" % _comment)
    reSepFields = re.compile(sepFields)
    # разделитель названия и значения переменной
    reSeparator = re.compile("=")

    def prepare(self):
        self.docObj = self.textToXML()
        self.doc = self.docObj.doc

    def postXML(self):
        """Последующая постобработка XML"""
        xmlFields = xpath.Evaluate("child::field", self.docObj.body)
        # Добавление переводов строк между полями
        for node in xmlFields:
            # Добавление перевода строк в если его нет между полями
            if (self.docObj.getTypeField(node) == "var" and
                    node.getprevious() is not None and
                    (self.docObj.getTypeField(node.getprevious()) not in
                        ("br", "comment"))):
                insertBefore(self.docObj.body, self.docObj.createField(
                    "br", [], "", [], False, False), node)

    def setDataField(self, txtLines, endtxtLines):
        """Создаем список объектов с переменными"""

        class fieldData:
            def __init__(self):
                self.name = False
                self.value = False
                self.comment = False
                self.br = False

        fields = []
        field = fieldData()
        z = 0
        for k in txtLines:
            textLine = k + endtxtLines[z]
            z += 1
            findComment = self.reComment.search(textLine)
            if not textLine.strip():
                field.br = textLine
                fields.append(field)
                field = fieldData()
            elif findComment:
                field.comment = textLine
                fields.append(field)
                field = fieldData()
            else:
                pars = textLine.strip()
                nameValue = self.reSeparator.split(pars)
                if len(nameValue) == 2:
                    name = nameValue[0]
                    value = nameValue[1].replace(self.sepFields, "")
                    field.name = name.replace(" ", "").replace("\t", "")
                    field.value = value
                    field.br = textLine
                    fields.append(field)
                    field = fieldData()
        return fields

    def textToXML(self):
        docObj = xmlDoc()
        docObj.createDoc(self.configName, self.configVersion)
        if self.text:
            nodeBody = docObj.getNodeBody()
            fields = self.splitToFields(self.text)
            for field in fields:
                if field.name is not False:
                    fieldXML = self.createFieldTerm(field.name,
                                                    field.value,
                                                    field.br, docObj)
                    nodeBody.append(fieldXML)
                    if field.br[-1] == "\n":
                        fieldXMLBr = docObj.createField(
                            "br", [], "", [], False, False)
                        nodeBody.append(fieldXMLBr)
                elif field.comment is not False:
                    fieldXML = docObj.createField(
                        "comment", [field.comment], "", [], False, False)
                    nodeBody.append(fieldXML)
                elif field.br is not False:
                    brText = field.br.replace("\n", "")
                    if brText:
                        fieldXML = docObj.createField(
                            'br', [brText], "", [], False, False)
                    else:
                        fieldXML = docObj.createField(
                            'br', [], "", [], False, False)
                    nodeBody.append(fieldXML)
        return docObj

    def join(self, procmailObj):
        """Объединяем конфигурации"""
        if isinstance(procmailObj, procmail):
            self.docObj.joinDoc(procmailObj.doc)
            self.postXML()
