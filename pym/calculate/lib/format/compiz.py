# -*- coding: utf-8 -*-

# Copyright 2008-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import re
from .samba import samba


class compiz(samba):
    """Класс для обработки конфигурационного файла типа compiz

    """
    _comment = "#"
    configName = "compiz"
    configVersion = "0.1"
    reHeader = re.compile("^[\t ]*\[[^\[\]]+\].*\n", re.M)
    reBody = re.compile(".+", re.M | re.S)
    reComment = re.compile("\s*%s.*" % _comment)
    reSeparator = re.compile("\s*=\s*")
    sepFields = "\n"
    reSepFields = re.compile(sepFields)

    def join(self, compizObj):
        """Объединяем конфигурации"""
        if isinstance(compizObj, compiz):
            self.docObj.joinDoc(compizObj.doc)
            self.postXML()
