#-*- coding: utf-8 -*-

# Copyright 2011-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from ..datavars import ReadonlyVariable

from . import X11
from . import locale
from . import env
from . import hardware
from . import linux
from . import net
from . import user
from . import system
from . import action
from ..cl_lang import setLocalTranslate
setLocalTranslate('cl_lib3',sys.modules[__name__])

section = "main"

class VariableClName(ReadonlyVariable):
    """
    Package name
    """
    value = "calculate-core"

class VariableClVer(ReadonlyVariable):
    """
    Package version
    """
    value = "3.7.6.8"
