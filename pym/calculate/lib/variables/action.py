# -*- coding: utf-8 -*-

# Copyright 2015-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from ..datavars import (Variable, VariableError)

from ..cl_lang import setLocalTranslate
from ..cl_template import Template

_ = lambda x: x
setLocalTranslate('cl_lib3', sys.modules[__name__])


class VariableAcCustomName(Variable):
    """
    Пользовательское действие для выполнения шаблонов
    """
    opt = ["ac_custom_name"]
    metavalue = "ACTION"
    type = "choice"
    value = ""
    untrusted = True

    def init(self):
        self.label = _("Custom action")
        self.help = _("custom action")

    def choice(self):
        old_action = self.Get('cl_action')
        old_name = self.Get('ac_custom_name')
        cl_templ = Template(self.parent, cltObj=False)
        try:
            cl_templ.applyTemplates()
        except Exception:
            pass
        finally:
            self.parent.Set('cl_action', old_action, force=True)
            self.parent.Set('ac_custom_name', old_name, force=True)
            cl_templ.closeFiles()
        return list(set((x[1] for x 
            in self.Get('cl_used_action') if x[0] == 'ac_custom_name')))

    def check(self, value):
        if value == '':
            raise VariableError(_("Need to specify an action name"))

    def raiseNothingValue(self):
        raise VariableError(_("No custom action on this system"))
