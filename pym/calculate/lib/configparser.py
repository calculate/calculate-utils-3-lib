# -*- coding: utf-8 -*-



from io import open
import configparser
# from collections import OrderedDict as _default_dict
# from .configparser_helpers import _ChainMap

__all__ = ["NoSectionError", "DuplicateOptionError", "DuplicateSectionError",
           "NoOptionError", "InterpolationError", "InterpolationDepthError",
           "InterpolationSyntaxError", "ParsingError",
           "MissingSectionHeaderError",
           "ConfigParser", "SafeConfigParser", "RawConfigParser",
           "DEFAULTSECT", "MAX_INTERPOLATION_DEPTH"]

DEFAULTSECT = "DEFAULT"

MAX_INTERPOLATION_DEPTH = 10

UTF8 = 'utf-8'
import os
from contextlib import contextmanager
from .utils.tools import LockError, Locker

#compat
NoSectionError = configparser.NoSectionError
DuplicateOptionError = configparser.DuplicateOptionError
DuplicateSectionError = configparser.DuplicateSectionError
NoOptionError = configparser.NoOptionError
InterpolationError = configparser.InterpolationError
InterpolationDepthError = configparser.InterpolationDepthError
InterpolationSyntaxError = configparser.InterpolationSyntaxError
ParsingError = configparser.ParsingError
MissingSectionHeaderError = configparser.MissingSectionHeaderError
ConfigParser = configparser.ConfigParser
SafeConfigParser = configparser.SafeConfigParser
RawConfigParser = configparser.RawConfigParser
Error = configparser.Error

class ConfigParserCaseSens(ConfigParser):
    """ConfigParser with case sensitivity keys"""

    def optionxform(self, optionstr):
        return optionstr

class ConfigParserLocked(ConfigParser):
    def __init__(self, filename, chmod=0o600):
        super().__init__(strict=False)
        self.filename = filename
        self.locker = Locker(fn=filename)
        self.chmod = chmod

    @contextmanager
    def lock_read(self):
        try:
            with self.locker:
                self.read(self.filename, encoding=UTF8)
                yield self
        except LockError:
            raise Error("Failed to lock %s" % self.filename)

    @contextmanager
    def lock_write(self):
        try:
            with self.locker:
                self.read(self.filename, encoding=UTF8)
                yield self
                with open(self.filename, 'w') as f:
                    os.chmod(self.filename, self.chmod)
                    self.write(f)
        except (OSError, IOError):
            raise Error("Failed to write %s" % self.filename)
        except LockError:
            raise Error("Failed to lock %s" % self.filename)


class ConfigParserCaseSensLocked(ConfigParserLocked):
    """ConfigParser with case sensitivity keys with locking"""

    def optionxform(self, optionstr):
        return optionstr