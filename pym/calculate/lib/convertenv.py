# -*- coding: utf-8 -*-

# Copyright 2008-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.


from .utils.text import convertStrListDict
from .configparser import ConfigParser
from collections import defaultdict


class convertEnv():
    """Конвертирование переменных из /var/calculate/remote/calculate.env"""
    envPath = "/var/calculate/remote/calculate.env"
    convertData = {
        "ur_organization": {"service": "mail", "replace": "organization"},
        "ur_signature": {"service": "mail", "replace": "signature"},
        "sr_mail_type": {"service": "mail", "replace": "type"},
        "sr_mail_send_host": {"service": "mail", "replace": "send_host"},
        "sr_mail_port": {"service": "mail", "replace": "port"},
        "sr_mail_crypt": {"service": "mail", "replace": "crypt"},
        "sr_mail_host": {"service": "mail", "replace": "host"},
        "sr_mail_send_port": {"service": "mail", "replace": "send_port"},
        "sr_mail_send_crypt": {"service": "mail", "replace": "send_crypt"},
        "ld_unix_dn": {"service": "unix", "replace": "dn"},
        "ld_bind_dn": {"service": "unix", "replace": "bind_dn"},
        "ld_bind_pw": {"service": "unix", "replace": "bind_pw"},
        "ld_services_dn": {"service": "ldap", "replace": "services_dn"},
        "ld_base_dn": {"service": "ldap", "replace": "base_dn"},
        "sr_jabber_crypt": {"service": "jabber", "replace": "crypt"},
        "sr_jabber_host": {"service": "jabber", "replace": "host"},
        "sr_jabber_port": {"service": "jabber", "replace": "port"},
        "sr_samba_host": {"service": "samba", "replace": "host"},
        "ld_samba_dn": {"service": "samba", "replace": "dn"},
        "cl_remote_ftp": {"service": "ftp", "replace": "host"},
        "sr_proxy_host": {"service": "proxy", "replace": "host"},
        "sr_proxy_port": {"service": "proxy", "replace": "port"}}
    convertVars = {}
    flagConverted = False

    def getVar(self, header, name):
        """Получаем сконвертированную переменную"""
        if not self.flagConverted:
            self.convertVars = self.convert()
            self.flagConverted = True
        return self.convertVars.get(header, {}).get(name, "")

    def convert(self):
        """Конвертирование переменных сервера в словарь новых переменных"""
        config = ConfigParser(strict=False)
        config.read(self.envPath, encoding="utf-8")
        if not config.has_section('client'):
            return {}

        convert_vars = defaultdict(dict)
        for k, v in config.items('client', raw=True):
            if k in self.convertData:
                service = self.convertData[k]['service']
                service_key = self.convertData[k]['replace']
                value = convertStrListDict(v)
                convert_vars[service][service_key] = value
        return convert_vars
