# -*- coding: utf-8 -*-

# Copyright 2018 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import re

class XorgConfig():
    """
    Объект получения параметров из xorg.conf подобного файла
    """
    section_pattern = '^Section "%s"\s*\n(.*?)^EndSection'

    class Section():
        value_pattern = '%s\s*"([^"]+)"'
        
        option_pattern = 'Option\s*"%s"\s*"([^"]+)"'

        def __init__(self, sectionname, content):
            self.sectionname = sectionname
            self.content = content

        @property
        def identifier(self):
            re_identifier = re.compile(self.value_pattern % "Identifier")
            m = re_identifier.search(self.content)
            if m:
                return m.group(1)
            return ""

        def values(self, name):
            re_val = re.compile(self.value_pattern % name)
            for value in re_val.findall(self.content):
                yield value

        def value(self, name):
            for val in self.values(name):
                return val
            return ""

        def option(self, optname):
            re_opt = re.compile(self.option_pattern % optname)
            m = re_opt.search(self.content)
            if m:
                return m.group(1)
            return ""

    def grab(self, data):
        """
        Отбросить пустые строки и комментарии
        """
        lines = (x.partition("#")[0].strip() for x in data.split("\n"))
        return "\n".join(x for x in lines if x)

    def __init__(self, content):
        self.content = self.grab(content)

    def get_sections(self, sectionname):
        """
        Получить все секции с заданным именем
        """
        re_section = re.compile(self.section_pattern % sectionname, re.M | re.S)
        for sectiondata in re_section.findall(self.content):
            yield self.Section(sectionname, sectiondata)

    def __contains__(self, sectionname):
        for x in self.get_sections(sectionname):
            return True
        else:
            return False
