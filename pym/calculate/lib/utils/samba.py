# -*- coding: utf-8 -*-

# Copyright 2018 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from .files import getProgPath, process
import re

_ = lambda x: x
from ..cl_lang import setLocalTranslate

setLocalTranslate('cl_lib3', sys.modules[__name__])

class SambaError(Exception):
    pass

class Samba():

    def nmblookup_ip_output(self, host):
        nmblookup = getProgPath("/usr/bin/nmblookup")
        if not nmblookup:
            raise SambaError(_("%s command not found"))
        return process(nmblookup, "-A", host).read()
    
    def get_server_domainname(self, host):
        """
        Reply codes
        http://pig.made-it.com/samba-protocols.html

        <00> 	Server Service
        <01>
        <03> 	Generic Machine Name (NetBIOS name)
        <1b> 	Domain Master Browser. A PDC should win the election to be the
                DMB, and should thus register the DOMAIN<1B> NetBIOS name.
        <1c> 	Server provides the NETLOGON-service. All domain controllers
                should register the DOMAIN<1C> name and Windows clients locate
                a network logon server by searching for this name.
        <1d> 	Local Master Browser. Receives browse lists from the 1B servers.
        <1e> 	Browser Election Service
        <20> 	Workstation on which the LanMan Server Service is running
        """
        data = self.nmblookup_ip_output(host)
        if "No reply" in data:
            raise SambaError(_("No reply from %s") % host)

        re_domainname = re.compile("^\s+(\S+)\s+<1c>\s+", re.M)
        m = re_domainname.search(data)
        if m:
            return m.group(1)
        raise SambaError(_("Failed to determine domain name on %s") % host)

    def password_check(self, username, password, server, resource, domain=None):
        """
        Подключиться к указанному samba ресурсу сервера по логину паролю
        """
        if not domain:
            domain = self.get_server_domainname(server)

        smbclient = getProgPath('/usr/sbin/smbclient')
        if not smbclient:
            raise SambaError(_("%s command not found") % "smbclient")
        p = process(smbclient, "-c", "ls", "-U", r"%s\%s"%(domain,username),
                    "//%s/%s" % (server, resource),
                    envdict={'PASSWD': password})
        return p.success()
