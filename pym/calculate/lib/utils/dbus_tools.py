# -*- coding: utf-8 -*-

# Copyright 2014-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.


import dbus
from os import path
from xml.etree import ElementTree


def get_dbus_path_tree(bus_name, bus=None):
    """
    Получить список путей объектов по имени шины
    """
    if not bus:
        bus = dbus.SessionBus()

    def get_child(objpath):
        obj = bus.get_object(bus_name, objpath)
        yield objpath
        if obj:
            for node in ElementTree.fromstring(obj.Introspect()):
                nodepath = path.join(objpath, node.get('name', ''))
                if node.tag == 'node':
                    for child in get_child(nodepath):
                        yield child

    try:
        for x in get_child('/'):
            yield x
    except dbus.DBusException:
        pass

def run_dbus_core(hostname, port):
    if hostname in ("127.0.0.1", "localhost"):
        try:
            from .ip import check_port
            if not check_port(hostname, port):
                bus =  dbus.SystemBus()
                DBUS_INTERFACE="org.calculate.CoreInterface"
                DBUS_NAME="org.calculate.Core"
                DBUS_OBJECT="/Core"
                try:
                    remote_object = bus.get_object(DBUS_NAME, DBUS_OBJECT)
                    remote_object.Start(port)
                except dbus.DBusException:
                    pass
        except ImportError:
            pass

def get_dbus_hostname():
    try:
        bus =  dbus.SystemBus()
        DBUS_INTERFACE="org.calculate.CoreInterface"
        DBUS_NAME="org.calculate.Core"
        DBUS_OBJECT="/Core"
        try:
            remote_object = bus.get_object(DBUS_NAME, DBUS_OBJECT)
            return str(remote_object.ServerHostname())
        except dbus.DBusException:
            pass
    except ImportError:
        pass
    return ""
