# -*- coding: utf-8 -*-

# Copyright 2014-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
from .printing import Print
from .palette import TextState
from .info import Terminal

Colors = TextState.Colors

from .converter import ConsoleCodes256Converter, XmlConverter
from .output import XmlOutput, ColorTerminal16Output, TerminalPositionOutput, \
    ColorTerminal256Output


def convert_console_to_xml(s):
    """Преобразовать вывод консоли в xml для внутреннего использования"""
    tmp = ConsoleCodes256Converter(output=XmlOutput()).transform(s)
    return  tmp


def get_color_print():
    """
    Получить объект для вывода текста в цвете
    """
    return Print(output=XmlOutput())


def get_terminal_output():
    return (ColorTerminal256Output()
            if Terminal().colors > 16
            else ColorTerminal16Output())


def get_terminal_print(printfunc=lambda x: x):
    """
    Получить объект для вывода в терминал
    """
    # TODO: возвращать объект 256 или 16 терминал в зависимости от параметров
    return Print(output=get_terminal_output(),
                 position_controller=TerminalPositionOutput(),
                 printfunc=printfunc)


def convert_xml_to_terminal(s):
    return XmlConverter(output=get_terminal_output()).transform(s)
