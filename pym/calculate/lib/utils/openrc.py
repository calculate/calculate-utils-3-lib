# -*- coding: utf-8 -*-

# Copyright 2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from .files import process
from os import path

from ..cl_lang import setLocalTranslate

setLocalTranslate('cl_lib3', sys.modules[__name__])

class OpenRCError(Exception):
    pass


class OpenRC():
    """
    Объект для управления openrc
    """
    runlevels_path = '/etc/runlevels'

    class Status:
        Started = 0
        Stopped = 3

    class Command():
        def __init__(self, command):
            self.command = command

        def __call__(self, *args):
            rc_service = process(self.command, *args, stderr=process.STDOUT)
            if rc_service.failed():
                raise OpenRCError(rc_service.read())

    rc_update = Command("/sbin/rc-update")
    rc_service = Command("/sbin/rc-service")

    def status(self, service):
        rc_service = process(self.rc_service.command, service, "status",
                             stderr=process.STDOUT)
        rc_service.success()
        return rc_service.pipe.returncode

    def start(self, service):
        if self.status(service) != self.Status.Started:
            self.rc_service(service, "start")

    def stop(self, service):
        if self.status(service) == self.Status.Started:
            self.rc_service(service, "stop")

    def is_autorun(self, service, runlevels=("default", "boot")):
        for runlevel in runlevels:
            check_path = path.join(self.runlevels_path, runlevel, service)
            if path.lexists(check_path):
                return True
        return False

    def add_default(self, service):
        self.is_autorun(service) or self.rc_update("add", service, "default")

    def del_default(self, service):
        self.is_autorun(service, ("default",)) and self.rc_update(
            "del", service, "default")
